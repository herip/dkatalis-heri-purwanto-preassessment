import java.util.*;

public class Main {
    private static List<String> sort(List<String> list) {
        List<String> set = new ArrayList<>(list);
        return set.stream().sorted().collect(Collectors.toList());
    }
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("5");
        list.add("2");
        list.add("7");
        System.out.println(sort(list));
    }
}
