import java.time.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        LocalDate localDateNow = LocalDate.parse("2019-08-25");
        LocalTime localTimeNow = LocalTime.parse("02:10:24");
        ZonedDateTime now = ZonedDateTime.of(localDateNow, localTimeNow, ZoneId.of("Asia/Bangkok"));
        LocalDate localDateOld = LocalDate.parse("2018-09-25");
        LocalTime localTimeOld = LocalTime.parse("02:10:24");
        ZonedDateTime oldDate = ZonedDateTime.of(localDateOld, localTimeOld, ZoneId.of("Asia/Bangkok"));
        Duration duration = Duration.between(oldDate, now);
        System.out.println("ISO-8601: " + duration);
        System.out.println("Minutes: " + duration.toMinutes());
    }
}
